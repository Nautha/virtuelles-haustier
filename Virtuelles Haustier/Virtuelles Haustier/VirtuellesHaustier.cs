﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtuellesHaustier
{
    abstract class VirtuellesHaustierKlasse
    {
        protected int alter;
        protected int hunger;
        protected int langeweile;
        protected string name;

        public int getALter()
        {
            return alter;
        }
        public int getHunger()
        {
            return hunger;
        }

        public int getLangeweile()
        {
            return langeweile;
        }

        public string getName()
        {
            return name;
        }

        public abstract int getGesundheit();

        public VirtuellesHaustierKlasse(string name) {
            this.alter = 0;
            this.hunger = 1;
            this.langeweile = 1;
            this.name = name;
        }

        public virtual void Warten() {
            this.alter++;
            this.hunger++;

            Random random = new Random();
            if(random.Next(0, 100) > 50) {
                this.langeweile++;
            }
        }

        public abstract void MedizinGeben();

        public abstract void Spielen();

        public virtual void Fuettern() {
            this.hunger = 1;
        }

        public virtual bool isAlive()
        {
            if (this.hunger < 5)
                if (this.langeweile < 5)
                    if (this.getGesundheit() < 5)
                        return true;
            return false;
        }
    }
}
