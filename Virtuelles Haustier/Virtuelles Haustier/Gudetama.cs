﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtuellesHaustier
{
    class Gudetama : VirtuellesHaustierKlasse
    {
        public Gudetama(string name) : base(name) {

        }

        public override int getGesundheit()
        {
            return 1;
        }

        public override void MedizinGeben()
        {
            throw new NotImplementedException();
        }

        public override void Spielen() {
            this.langeweile--;
        }
    }
}
