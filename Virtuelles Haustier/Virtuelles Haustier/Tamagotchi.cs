﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtuellesHaustier
{
    class Tamagotchi : VirtuellesHaustierKlasse
    {
        protected int gesundheit;

        public Tamagotchi(string name) : base(name) {
            this.gesundheit = 1;
        }

        public override int getGesundheit()
        {
            return gesundheit;
        }

        public override void Warten() {
            base.Warten();

            if(this.hunger > 3) {
                Random random = new Random();
                if (random.Next(0, 100) > 50) {
                    this.gesundheit++;
                }
            }
        }

        public override void MedizinGeben() {
            if(this.gesundheit == 1) {
                this.gesundheit = 3;
            } else {
                gesundheit -= 2;

                if(this.gesundheit < 1) {
                    this.gesundheit = 1;
                }
            }
        }

        public override void Spielen() {
            this.langeweile = 1;
            this.hunger++;
        }
    }
}
